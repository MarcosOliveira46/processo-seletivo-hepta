# Execução

Para a execução, é necessário apenas 
1. trocar o password do arquivo persistence.xml, levando em consideração que o nome do banco de dados criado seja 
"funcionarios_prova"

2. Possuir mysql versão 8.0.17, ou trocar o connector em casos de versões diferentes

3. Foi necessário a utilização do eclipse (utilizei a versão 2022), tomcat 9 e jdk 8

#O que não será capaz de ser feito
1. Embora esteja pronta a lógica do HIbernet para a execução de comandos simples do sql, não está funcionando 
atualizar registros e apagar registros, devido algum problema que não fui capaz de resolver. Essas modificações foram feitas
pelo ajax do jquery, utilizando a Index.js rederizada já no html


# Scripts utilizado para a execução

CREATE DATABASE funcionarios_prova;

create table Setor(
	ID_SETOR INT not null PRIMARY KEY,
    NOME varchar(100)
);

create table Funcionario(
	ID_FUNCIONARIO INT not null PRIMARY KEY ,
    NOME VARCHAR(100),
    NU_SALARIO INT,
    DS_EMAIL VARCHAR(100),
    NU_IDADE INT(3),
    FK_SETOR INT,
    CONSTRAINT FK_FUNCIONARIO_SETOR FOREIGN KEY(FK_SETOR) REFERENCES Setor(ID_SETOR)
);


INSERT INTO Setor VALUES (1, 'Tecnologia da Informação');
INSERT INTO Setor VALUES (2, 'Contabilidade');
INSERT INTO `funcionarios_prova`.`funcionario` (`ID_FUNCIONARIO`, `NOME`, `NU_SALARIO`, `DS_EMAIL`, `NU_IDADE`, `FK_SETOR`) VALUES ('1', 'Marcos', '3000', 'markiinjn@gmail.com', '20', '1');
INSERT INTO `funcionarios_prova`.`funcionario` (`ID_FUNCIONARIO`, `NOME`, `NU_SALARIO`, `DS_EMAIL`, `NU_IDADE`, `FK_SETOR`) VALUES ('2', 'Pedro', '5000', 'pedro@gmail.com', '18', '1');
INSERT INTO `funcionarios_prova`.`funcionario` (`ID_FUNCIONARIO`, `NOME`, `NU_SALARIO`, `DS_EMAIL`, `NU_IDADE`, `FK_SETOR`) VALUES ('3', 'Joao', '2000', 'joao@gmail.com', '18', '2');






