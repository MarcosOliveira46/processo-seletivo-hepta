var inicio = new Vue({
	el:"#inicio",
    data: {
        lista: []
    },
    created: function(){
        let vm =  this;
        vm.listarFuncionarios();
    },
    methods:{
	//Busca os itens para a lista da primeira página
        listarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rest/funcionarios")
			.then(response => {vm.lista = response.data;
			}).catch(function (error) {
				console.log(error)
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços");
			}).finally(function() {
			});
		},
		mostraAlertaErro: function(erro, mensagem){
			console.log(erro);
			alert(mensagem);
		}
		/*,
		greet: function () {
	      alert('Olá ')
	      
	    },
		greet2: function () {
	      alert('Olá 2')
	      
	    }*/
    }
});
var editar = new Vue({
	el:"#editar",
    data: {
        lista: []
    },
    created: function(){
        let vm =  this;
        vm.listarFuncionarios();
    },
    methods:{
	//Busca os itens para a lista da primeira página
        listarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rest/funcionarios")
			.then(response => {vm.lista = response.data;
			}).catch(function (error) {
				console.log(error)
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços");
			}).finally(function() {
			});
		},
		mostraAlertaErro: function(erro, mensagem){
			console.log(erro);
			alert(mensagem);
		}
		/*,
		greet: function () {
	      alert('Olá ')
	      
	    },
		greet2: function () {
	      alert('Olá 2')
	      
	    }*/
    }
});

function greet(e){
	
	if($(e.target).hasClass('pencil')){
		
		let campos = $(e.target).parent().siblings('td')
		$(e.target).html('&#x2714')
		$(e.target).toggleClass('pencil')
		for(var i = 0; i < campos.length; i++){
			let valor = $(campos[i]).html()
			let name = i == 0 ? 'nome' : (i == 1 ? 'setor' : (i == 2 ? 'salario' : (i == 3 ? 'email' : (i == 4 ? 'idade' : 'undefined'))))
			$(campos[i]).html(`<input type="text" class="form-control col-xs-" value="${valor}" name="${name}" />`)
			
		}
		
	}else{
		let campos = $(e.target).parent().siblings('td').find('input')
		$(e.target).html('&#9998;')
		$(e.target).toggleClass('pencil')
		for(var i = 0; i < campos.length; i++){
			let valor = $(campos[i]).val()
			console.log(campos[i])
			//let name = i == 0 ? 'nome' : (i == 1 ? 'setor' : (i == 2 ? 'salario' : (i == 3 ? 'email' : (i == 4 ? 'idade' : 'undefined'))))
			$(campos[i].closest('td')).html(valor)
			
		}
		
		$.ajax({
	        type: 'PUT',
	        url: '/funcionarios/rest/funcionarios/atualiza/' + campos[5],
	        data: {nome: campos[0], setor: campos[1], salario: campos[2], email: campos[3], idade: campos[4]},
	        dataType: 'json',
	        success: dados => { //Quando você faz dessa maneira, você está acessando o responsetext do app.php
	            console.log(dados);
	        },
	        error: erro => { console.log('erro: ' + erro) }
    	})
    	
		//$.post('/funcionarios/rest/funcionarios/'+ campos[5],{nome: campos[0], setor: campos[1], salario: campos[2], email: campos[3], idade: campos[4]} )
    	
	}
	
	
}

function deletar(e){
	let campos = $(e.target).parent().siblings('td')
	$.ajax({
	        type: 'DELETE',
	        url: '/funcionarios/rest/funcionarios/deletar/' + campos[5],
	        success: dados => { //Quando você faz dessa maneira, você está acessando o responsetext do app.php
	            console.log(dados);
	        },
	        error: erro => { console.log('erro: ' + erro) }
    	})
	$(e.target).closest('tr').remove()
	
}