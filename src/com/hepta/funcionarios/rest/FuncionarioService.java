package com.hepta.funcionarios.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.persistence.FuncionarioDAO;

@Path("/funcionarios")
public class FuncionarioService {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private FuncionarioDAO dao;

    public FuncionarioService() {
        dao = new FuncionarioDAO();
    }

    protected void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Adiciona novo Funcionario
     * 
     * @param Funcionario: Novo Funcionario
     * @return response 200 (OK) - Conseguiu adicionar
     */
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response FuncionarioCreate(Funcionario Funcionario) {
        return Response.status(Status.NOT_IMPLEMENTED).build();
    }

    /**
     * Lista todos os Funcionarios
     * 
     * @return response 200 (OK) - Conseguiu listar
     */
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response FuncionarioRead() {
        List<Funcionario> Funcionarios = new ArrayList<>();
        try {
            Funcionarios = dao.getAll();
            /*
            for(Integer i = 0; i < Funcionarios.size(); i++) {
            	
                System.out.print("Id: " + Funcionarios.get(i).getId());	
                System.out.print("Nome: " + Funcionarios.get(i).getNome());
                
            }
            
            String action = request.getServletPath();
            System.out.print(action);
            */
        } catch (Exception e) {
            return null;
        }
        
        GenericEntity<List<Funcionario>> entity = new GenericEntity<List<Funcionario>>(Funcionarios) {
        };
        
        return Response.status(Status.OK).entity(entity).build();
    }

    /**
     * Atualiza um Funcionario
     * 
     * @param id:          id do Funcionario
     * @param Funcionario: Funcionario atualizado
     * @return response 200 (OK) - Conseguiu atualizar
     */
    @Path("atualiza/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Response FuncionarioUpdate( Funcionario Funcionario) {
    	Funcionario funcionarioAtualizado = new Funcionario(); 
    	try {

        	 funcionarioAtualizado = dao.update(Funcionario);
    	}
    	catch(Exception E) {
    		return null;
    	}
    	GenericEntity<Funcionario> entity = new GenericEntity<Funcionario>(funcionarioAtualizado) {
        };
        //System.out.print("ID; " + Funcionario.getNome());
        return Response.status(Status.OK).entity(entity).build();
    }

    /**
     * Remove um Funcionario
     * 
     * @param id: id do Funcionario
     * @return response 200 (OK) - Conseguiu remover
     */
    @Path("deletar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @DELETE
    public Response FuncionarioDelete(@PathParam("id") Integer id) { 
    	try {

        	 dao.delete(id);
        	 return Response.status(Status.OK).build();
    	}
    	catch(Exception E) {
    		return Response.status(Status.NOT_IMPLEMENTED).build();
    	}
        //System.out.print("ID; " + Funcionario.getNome());
    	
    }

    /**
     * Métodos simples apenas para testar o REST
     * @return
     * @throws IOException 
     */
    @Path("/teste")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String TesteJersey() throws IOException {
    	return "Funcionando.";
    	//List<Funcionario> funcionarios = FuncionarioRead();
        //response.sendRedirect("../../../funcionarios");
    }

}
